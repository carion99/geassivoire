<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\QuestionnaireFormRepository")
 */
class QuestionnaireForm
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codeQte;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="integer")
     */
    private $dureeValidite;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $domaineActivite;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codeClient;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodeQte(): ?string
    {
        return $this->codeQte;
    }

    public function setCodeQte(string $codeQte): self
    {
        $this->codeQte = $codeQte;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDureeValidite(): ?int
    {
        return $this->dureeValidite;
    }

    public function setDureeValidite(int $dureeValidite): self
    {
        $this->dureeValidite = $dureeValidite;

        return $this;
    }

    public function getDomaineActivite(): ?string
    {
        return $this->domaineActivite;
    }

    public function setDomaineActivite(string $domaineActivite): self
    {
        $this->domaineActivite = $domaineActivite;

        return $this;
    }

    public function getCodeClient(): ?string
    {
        return $this->codeClient;
    }

    public function setCodeClient(string $codeClient): self
    {
        $this->codeClient = $codeClient;

        return $this;
    }
}
