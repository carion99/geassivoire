<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\QuestionFormRepository")
 */
class QuestionForm
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codeQuestion;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codeQte;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $intitule;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbReponse;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $obligation;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodeQuestion(): ?string
    {
        return $this->codeQuestion;
    }

    public function setCodeQuestion(string $codeQuestion): self
    {
        $this->codeQuestion = $codeQuestion;

        return $this;
    }

    public function getCodeQte(): ?string
    {
        return $this->codeQte;
    }

    public function setCodeQte(string $codeQte): self
    {
        $this->codeQte = $codeQte;

        return $this;
    }

    public function getIntitule(): ?string
    {
        return $this->intitule;
    }

    public function setIntitule(string $intitule): self
    {
        $this->intitule = $intitule;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getNbReponse(): ?int
    {
        return $this->nbReponse;
    }

    public function setNbReponse(int $nbReponse): self
    {
        $this->nbReponse = $nbReponse;

        return $this;
    }

    public function getObligation(): ?string
    {
        return $this->obligation;
    }

    public function setObligation(string $obligation): self
    {
        $this->obligation = $obligation;

        return $this;
    }
}
