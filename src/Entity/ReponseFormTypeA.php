<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ReponseFormTypeARepository")
 */
class ReponseFormTypeA
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codeReponse;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codeQuestion;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $intitule;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodeReponse(): ?string
    {
        return $this->codeReponse;
    }

    public function setCodeReponse(string $codeReponse): self
    {
        $this->codeReponse = $codeReponse;

        return $this;
    }

    public function getCodeQuestion(): ?string
    {
        return $this->codeQuestion;
    }

    public function setCodeQuestion(string $codeQuestion): self
    {
        $this->codeQuestion = $codeQuestion;

        return $this;
    }

    public function getIntitule(): ?string
    {
        return $this->intitule;
    }

    public function setIntitule(string $intitule): self
    {
        $this->intitule = $intitule;

        return $this;
    }
}
