<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\QuestionnaireTestRepository")
 */
class QuestionnaireTest
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codeQte;

    /**
     * @ORM\Column(type="integer")
     */
    private $dureeValidite;

    /**
     * @ORM\Column(type="integer")
     */
    private $dureeTest;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbParticipant;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codeClient;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodeQte(): ?string
    {
        return $this->codeQte;
    }

    public function setCodeQte(string $codeQte): self
    {
        $this->codeQte = $codeQte;

        return $this;
    }

    public function getDureeValidite(): ?int
    {
        return $this->dureeValidite;
    }

    public function setDureeValidite(int $dureeValidite): self
    {
        $this->dureeValidite = $dureeValidite;

        return $this;
    }

    public function getDureeTest(): ?int
    {
        return $this->dureeTest;
    }

    public function setDureeTest(int $dureeTest): self
    {
        $this->dureeTest = $dureeTest;

        return $this;
    }

    public function getNbParticipant(): ?int
    {
        return $this->nbParticipant;
    }

    public function setNbParticipant(int $nbParticipant): self
    {
        $this->nbParticipant = $nbParticipant;

        return $this;
    }

    public function getCodeClient(): ?string
    {
        return $this->codeClient;
    }

    public function setCodeClient(string $codeClient): self
    {
        $this->codeClient = $codeClient;

        return $this;
    }
}
