<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\QuestionnaireRepository")
 */
class Questionnaire
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codeQte;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codeClient;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titre;


    /**
     * @ORM\Column(type="integer")
     */
    private $nbQuestion;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $confirmation;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $styleDeFond;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $couleurDeFond;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateRegister;

    public function __construct(){
        $this->setStyleDeFond("geasscolor");
        $this->setCouleurDeFond("");
        $this->setDateRegister(new \DateTime());
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodeQte(): ?string
    {
        return $this->codeQte;
    }

    public function setCodeQte(string $codeQte): self
    {
        $this->codeQte = $codeQte;

        return $this;
    }

    public function getCodeClient(): ?string
    {
        return $this->codeClient;
    }

    public function setCodeClient(string $codeClient): self
    {
        $this->codeClient = $codeClient;

        return $this;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }


    public function getNbQuestion(): ?int
    {
        return $this->nbQuestion;
    }

    public function setNbQuestion(int $nbQuestion): self
    {
        $this->nbQuestion = $nbQuestion;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getConfirmation(): ?string
    {
        return $this->confirmation;
    }

    public function setConfirmation(string $confirmation): self
    {
        $this->confirmation = $confirmation;

        return $this;
    }

    public function getStyleDeFond(): ?string
    {
        return $this->styleDeFond;
    }

    public function setStyleDeFond(string $styleDeFond): self
    {
        $this->styleDeFond = $styleDeFond;

        return $this;
    }

    public function getCouleurDeFond(): ?string
    {
        return $this->couleurDeFond;
    }

    public function setCouleurDeFond(string $couleurDeFond): self
    {
        $this->couleurDeFond = $couleurDeFond;

        return $this;
    }

    public function getDateRegister(): ?\DateTimeInterface
    {
        return $this->dateRegister;
    }

    public function setDateRegister(\DateTimeInterface $dateRegister): self
    {
        $this->dateRegister = $dateRegister;

        return $this;
    }

}
