<?php

namespace App\Repository;

use App\Entity\ReponseFormTypeA;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ReponseFormTypeA|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReponseFormTypeA|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReponseFormTypeA[]    findAll()
 * @method ReponseFormTypeA[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReponseFormTypeARepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ReponseFormTypeA::class);
    }

    // /**
    //  * @return ReponseFormTypeA[] Returns an array of ReponseFormTypeA objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ReponseFormTypeA
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
