<?php

namespace App\Repository;

use App\Entity\QuestionTest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method QuestionTest|null find($id, $lockMode = null, $lockVersion = null)
 * @method QuestionTest|null findOneBy(array $criteria, array $orderBy = null)
 * @method QuestionTest[]    findAll()
 * @method QuestionTest[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QuestionTestRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, QuestionTest::class);
    }

    // /**
    //  * @return QuestionTest[] Returns an array of QuestionTest objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('q.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?QuestionTest
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
