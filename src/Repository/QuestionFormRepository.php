<?php

namespace App\Repository;

use App\Entity\QuestionForm;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method QuestionForm|null find($id, $lockMode = null, $lockVersion = null)
 * @method QuestionForm|null findOneBy(array $criteria, array $orderBy = null)
 * @method QuestionForm[]    findAll()
 * @method QuestionForm[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QuestionFormRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, QuestionForm::class);
    }

    // /**
    //  * @return QuestionForm[] Returns an array of QuestionForm objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('q.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?QuestionForm
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
