<?php

namespace App\Repository;

use App\Entity\ReponseTest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ReponseTest|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReponseTest|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReponseTest[]    findAll()
 * @method ReponseTest[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReponseTestRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ReponseTest::class);
    }

    // /**
    //  * @return ReponseTest[] Returns an array of ReponseTest objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ReponseTest
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
