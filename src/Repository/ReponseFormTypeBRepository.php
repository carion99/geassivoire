<?php

namespace App\Repository;

use App\Entity\ReponseFormTypeB;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ReponseFormTypeB|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReponseFormTypeB|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReponseFormTypeB[]    findAll()
 * @method ReponseFormTypeB[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReponseFormTypeBRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ReponseFormTypeB::class);
    }

    // /**
    //  * @return ReponseFormTypeB[] Returns an array of ReponseFormTypeB objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ReponseFormTypeB
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
