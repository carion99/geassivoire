<?php

namespace App\Repository;

use App\Entity\QuestionnaireSondage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method QuestionnaireSondage|null find($id, $lockMode = null, $lockVersion = null)
 * @method QuestionnaireSondage|null findOneBy(array $criteria, array $orderBy = null)
 * @method QuestionnaireSondage[]    findAll()
 * @method QuestionnaireSondage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QuestionnaireSondageRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, QuestionnaireSondage::class);
    }

    // /**
    //  * @return QuestionnaireSondage[] Returns an array of QuestionnaireSondage objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('q.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?QuestionnaireSondage
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
