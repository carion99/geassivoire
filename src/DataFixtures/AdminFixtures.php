<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

use App\Entity\User;
use App\Entity\Admin;

class AdminFixtures extends Fixture
{
	private $encoder;

	public function __construct(UserPasswordEncoderInterface $encoder)
	{
		$this->encoder = $encoder;
	}

    public function load(ObjectManager $manager)
    {
        $admin1 = new Admin();
        $user1 = new User();
        $admin2 = new Admin();
        $user2 = new User();

            function genCode($motcle)
               {
                   $nb  = rand(1000000, 9999999);
                   $code = $motcle.'-'.$nb;

                   return $code;
               }

        $email1 = 'mediyann99@gmail.com';
        $nom1 = 'medi';
        $prenom1 = 'yann michael';
        $status1 = 'administrateur';
        $code1 = genCode("admin");
        $password1 = "12345678";
        $hashpasword1 = $this->encoder->encodePassword($user1, '12345678');
        $date1 = new \Datetime();

        $user1->setUsername($email1);
        $user1->setRoles(['ROLE_ADMIN']);
        $user1->setPassword($hashpasword1);
        $user1->setLastname($nom1);
        $user1->setFirstname($prenom1);
        $user1->setStatus($status1);
        $user1->setCodeUser($code1);

        $admin1->setCodeAdmin($code1)
        	  ->setNom($nom1)
        	  ->setPrenom($prenom1)
        	  ->setEmail($email1)
        	  ->setMotdepasse($hashpasword1)
        	  ->setDateregister($date1);

		$manager->persist($user1);
        $manager->persist($admin1);
        


        $email2 = 'etiemele01@gmail.com';
        $nom2 = 'tiemele';
        $prenom2 = 'eliud';
        $status2 = 'administrateur';
        $code2 = genCode("admin");
        $password2 = "12345678";
        $hashpasword2 = $this->encoder->encodePassword($user2, '12345678');
        $date2 = new \Datetime();

        $user2->setUsername($email2);
        $user2->setRoles(['ROLE_ADMIN']);
        $user2->setPassword($hashpasword2);
        $user2->setLastname($nom2);
        $user2->setFirstname($prenom2);
        $user2->setStatus($status2);
        $user2->setCodeUser($code2);

        $admin2->setCodeAdmin($code2)
        	  ->setNom($nom2)
        	  ->setPrenom($prenom2)
        	  ->setEmail($email2)
        	  ->setMotdepasse($hashpasword2)
        	  ->setDateregister($date2);

		$manager->persist($user2);
		$manager->persist($admin2);

		$manager->flush();
    }
}
