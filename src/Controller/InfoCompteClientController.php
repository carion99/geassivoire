<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class InfoCompteClientController extends AbstractController
{
    /**
     * @Route("/compte/infos", name="info_compte_client")
     */
    public function index()
    {
        return $this->render('info_compte_client/index.html.twig');
    }
}
