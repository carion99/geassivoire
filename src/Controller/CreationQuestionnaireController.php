<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;

use App\Entity\Questionnaire;
use App\Entity\QuestionnaireSondage;
use App\Entity\QuestionnaireTest;
use App\Entity\QuestionnaireForm;

use App\Entity\QuestionSondage;
use App\Entity\QuestionTest;
use App\Entity\QuestionForm;


use App\Entity\ReponseSondage;
use App\Entity\ReponseTest;
use App\Entity\ReponseFormTypeA;
use App\Entity\ReponseFormTypeB;

class CreationQuestionnaireController extends AbstractController
{
    /**
     * @Route("/compte/nouveau", name="nouveau")
     */
    public function Nouveau()
    {
        return $this->render('creation_questionnaire/index.html.twig');
    }

    /**
     * @Route("/compte/nouveau/sond/{etape}", name="sond_qte")
     */
    public function QteSondage($etape = 1, Request $request, ObjectManager $manager)
    {

        function isCorrect($var){
            $var = htmlspecialchars($var);
            //$var = mysql_real_escape_string($var);

            return $var;
        }

        function genCode($mot, $nb_debut, $nb_fin){
            $nb = rand($nb_debut, $nb_fin);
            $code = $mot."-".$nb;

            return $code;
        }

        if ($etape == 1) {

            if ($request->request->count() > 0) {
                $qte = new Questionnaire();
                $qte_sond = new QuestionnaireSondage();

                $codeqte = genCode("qtesond", 100000, 999999);
                $type = "sondage";
                $titre = isCorrect($request->request->get('titre'));
                $nbquestion = intval($request->request->get('nbquestion'));
                $validite = intval($request->request->get('validite'));
                $style = $request->request->get('style');
                $description = isCorrect($request->request->get('description'));
                $confirmation = isCorrect($request->request->get('confirmation'));
                if ($confirmation == "") {
                    $confirmation = "Merci bien, votre/vos réponse(s) ont bien été enregistrée(s)";
                }
                $codeclient = $this->getUser()->getCodeUser();

                $qte->setCodeQte($codeqte)
                    ->setCodeClient($codeclient)
                    ->setTitre($titre)
                    ->setNbQuestion($nbquestion)
                    ->setType($type)
                    //->setStyleDeFond($style)
                    ->setConfirmation($confirmation);
                $qte_sond->setCodeQte($codeqte)
                        ->setCodeClient($codeclient)
                        ->setDescription($description)
                        ->setDureeValidite($validite);
                $manager->persist($qte);
                $manager->persist($qte_sond);
                $manager->flush();

                return $this->redirectToRoute('sond_qte', ['etape' => 2]);
            }
            else{
                return $this->render('creation_questionnaire/sondage.html.twig', [
                    'etape' => $etape
                ]);
            }




        }elseif ($etape == 2) {

            $codeuser = $this->getUser()->getCodeUser();
            $repo = $manager->getRepository(Questionnaire::class);
            $qtes = $repo->findByCodeClient($codeuser);

            $dernier_qte = end($qtes);
            $codeqte = $dernier_qte->getCodeQte();
            $nbquestion = $dernier_qte->getNbQuestion();

            for ($i=0; $i < $nbquestion; $i++) {
                $j = $i + 1;
                $noquestion = $j;
                $name_intitule = "intitule".$j;
                $name_nbreponse= "nbreponse".$j;
                $name_type     = "typequestion".$j;

                $question["noquestion"]    = $noquestion;
                $question["intitule"]       = $name_intitule;
                $question["nbreponse"]      = $name_nbreponse;
                $question["typequestion"]   = $name_type;

                $questions[$i] = $question;

            }



            if ($request->request->count() > 0) {

                for ($i=0; $i < $nbquestion; $i++) {
                    $j = $i + 1;
                    $name_intitule = "intitule".$j;
                    $name_nbreponse= "nbreponse".$j;
                    $name_type     = "typequestion".$j;

                    $question_sond = new QuestionSondage();
                    $codequestion = genCode("questsond", 100000000, 999999999);
                    $intitule = isCorrect($request->request->get($name_intitule));
                    $nbreponse = intval($request->request->get($name_nbreponse));
                    $type = $request->request->get($name_type);

                    $question_sond->setCodeQuestion($codequestion)
                                ->setCodeQte($codeqte)
                                ->setIntitule($intitule)
                                ->setNbReponse($nbreponse)
                                ->setType($type);
                    $manager->persist($question_sond);
                    $manager->flush();
                }

                return $this->redirectToRoute('sond_qte', ['etape' => 3]);

            }
            if (!isset($questions)) {
                $questions = [];
            }

            return $this->render('creation_questionnaire/sondage.html.twig', [
                'etape' => $etape,
                'questions' => $questions
            ]);

        }elseif ($etape == 3) {

            $codeuser = $this->getUser()->getCodeUser();
            $repo1 = $manager->getRepository(Questionnaire::class);
            $qtes = $repo1->findByCodeClient($codeuser);

            $dernier_qte = end($qtes);
            $codeqte = $dernier_qte->getCodeQte();
            $nbquestion = $dernier_qte->getNbQuestion();

            $repo2 = $manager->getRepository(QuestionSondage::class);
            $quests = $repo2->findByCodeQte($codeqte);
            $nbquests = count($quests);

            if ($nbquests == $nbquestion) {
                for ($i=0; $i < $nbquestion; $i++) {
                    $quest = $quests[$i];
                    $intitule_question = $quest->getIntitule();
                    $nbreponse = $quest->getNbReponse();
                    for ($j=0; $j < $nbreponse; $j++) {
                        $a = $i + 1;
                        $b = $j + 1;
                        $name_intitule = "intitule".$a.$b;
                        $noquestion = $a;
                        $noreponse  = $b;

                        //$rep["intitule_question"] = $intitule_question;
                        $rep["noquestion"] = $noquestion;
                        $rep["noreponse"] = $noreponse;
                        $rep["intitule_reponse"] = $name_intitule;

                        $repquest[$j] = $rep;

                        $inforep["noquestion"] = $noquestion;
                        $inforep["intitule_question"] = $intitule_question;
                        $inforep["reponse"] = $repquest;

                        //$repquest["intitule_question"] = $intitule_question;
                    }
                    $reponses[$i] = $inforep;
                }
            }else{
                $reponses = [];
            }

            if ($request->request->count() > 0) {
                for ($i=0; $i < $nbquestion; $i++) {
                    $quest = $quests[$i];
                    $codequestion = $quest->getCodeQuestion();
                    $nbreponse = $quest->getNbReponse();
                    for ($j=0; $j < $nbreponse; $j++) {
                        $a = $i + 1;
                        $b = $j + 1;
                        $name_intitule = "intitule".$a.$b;

                        $reponse = new ReponseSondage();

                        $intitule = isCorrect($request->request->get($name_intitule));
                        $codereponse = genCode("repsond", 100000000000, 999999999999);
                        $default_score = 0;

                        $reponse->setCodeReponse($codereponse)
                                ->setCodeQuestion($codequestion)
                                ->setIntitule($intitule)
                                ->setScore($default_score);
                        $manager->persist($reponse);
                        $manager->flush();
                    }
                }
                return $this->redirectToRoute('sond_qte', ['etape' => 4]);
            }

            return $this->render('creation_questionnaire/sondage.html.twig', [
                'etape' => $etape,
                'reponses' => $reponses
            ]);
        }elseif ($etape == 4) {

            $codeuser = $this->getUser()->getCodeUser();
            $repo = $manager->getRepository(Questionnaire::class);
            $qtes = $repo->findByCodeClient($codeuser);

            $dernier_qte = end($qtes);
            $codeqte = $dernier_qte->getCodeQte();

            $style = $dernier_qte->getStyleDeFond();

            if ($style == "geasscolor") {
                $typestyle["nb"] = 1;
                $color[0] = "red";
                $color[1] = "green";
                $color[2] = "blue-gradient";
                $color[3] = "orange";
                $color[4] = "yellow";
                $color[5] = "grey";
                $color[6] = "pink";
                $color[7] = "brown";
                $color[9] = "warm-flame-gradient";
                $color[10] = "night-fade-gradient";
                $color[11] = "spring-warmth-gradient";
                $color[12] = "juicy-peach-gradient";
                $color[13] = "young-passion-gradient";
                $color[14] = "rainy-ashville-gradient";
                $color[15] = "sunny-morning-gradient";
                $color[16] = "lady-lips-gradient";
                $color[17] = "winter-neva-gradient";
                $color[18] = "frozen-dreams-gradient";
                $color[19] = "dusty-grass-gradient";
                $color[20] = "tempting-azure-gradient";
                $color[21] = "heavy-rain-gradient";
                $color[22] = "amy-crisp-gradient";
                $color[23] = "mean-fruit-gradient";
                $color[24] = "deep-blue-gradient";
                $color[25] = "ripe-malinka-gradient";
                $color[26] = "cloudy-knoxville-gradient";
                $color[28] = "morpheus-den-gradient";
                $color[29] = "rare-wind-gradient";
                $color[30] = "near-moon-gradient";
                $color[31] = "peach-gradient";
                $color[32] = "aqua-gradient";
                $color[33] = "purple-gradient";

                $typestyle["color"] = $color;

            }elseif ($style == "geasspicture") {
                $typestyle["nb"] = 2;
            }else {
                return $this->redirectToRoute('logout');
            }

            return $this->render('creation_questionnaire/sondage.html.twig', [
                'etape' => $etape,
                'typestyle' => $typestyle
            ]);
        }else {
            return $this->redirectToRoute('compte');
        }

/*        return $this->render('creation_questionnaire/sondage.html.twig', [
            'etape' => $etape
        ]);
*/
    }

    /**
     * @Route("/compte/nouveau/test/{etape}", name="test_qte")
     */
    public function QteTest($etape = 1, Request $request, ObjectManager $manager)
    {
        function isCorrect($var){
            $var = htmlspecialchars($var);
            //$var = mysql_real_escape_string($var);

            return $var;
        }

        function genCode($mot, $nb_debut, $nb_fin){
            $nb = rand($nb_debut, $nb_fin);
            $code = $mot."-".$nb;

            return $code;
        }

        if ($etape == 1) {

            if ($request->request->count() > 0) {
                $qte = new Questionnaire();
                $qte_test = new QuestionnaireTest();

                $codeqte = genCode("qtetest", 100000, 999999);
                $type = "test";
                $titre = isCorrect($request->request->get('titre'));
                $nbquestion = intval($request->request->get('nbquestion'));
                $nbpart = intval($request->request->get('nbpart'));
                $validite = intval($request->request->get('validite'));
                $evaluation = intval($request->request->get('evaluation'));
                $style = $request->request->get('style');
                $description = isCorrect($request->request->get('description'));
                $confirmation = isCorrect($request->request->get('confirmation'));
                if ($confirmation == "") {
                    $confirmation = "Merci bien, votre/vos réponse(s) ont bien été enregistrée(s)";
                }
                $codeclient = $this->getUser()->getCodeUser();

                $qte->setCodeQte($codeqte)
                    ->setCodeClient($codeclient)
                    ->setTitre($titre)
                    ->setNbQuestion($nbquestion)
                    ->setType($type)
                    ->setConfirmation($confirmation);
                $qte_test->setCodeQte($codeqte)
                        ->setCodeClient($codeclient)
                        ->setDureeValidite($validite)
                        ->setDureeTest($evaluation)
                        ->setNbParticipant($nbpart);
                $manager->persist($qte);
                $manager->persist($qte_test);
                $manager->flush();

                return $this->redirectToRoute('test_qte', ['etape' => 2]);
            }else{
                return $this->render('creation_questionnaire/test.html.twig', [
                    'etape' => $etape
                ]);
            }
        }elseif ($etape == 2) {
            $codeuser = $this->getUser()->getCodeUser();
            $repo = $manager->getRepository(Questionnaire::class);
            $qtes = $repo->findByCodeClient($codeuser);

            $dernier_qte = end($qtes);
            $codeqte = $dernier_qte->getCodeQte();
            $nbquestion = $dernier_qte->getNbQuestion();

            for ($i=0; $i < $nbquestion; $i++) {
                $j = $i + 1;
                $noquestion = $j;
                $name_intitule = "intitule".$j;
                $name_nbreponse= "nbreponse".$j;
                $name_type     = "typequestion".$j;

                $question["noquestion"]    = $noquestion;
                $question["intitule"]       = $name_intitule;
                $question["nbreponse"]      = $name_nbreponse;
                $question["typequestion"]   = $name_type;

                $questions[$i] = $question;

            }



            if ($request->request->count() > 0) {

                for ($i=0; $i < $nbquestion; $i++) {
                    $j = $i + 1;
                    $name_intitule = "intitule".$j;
                    $name_nbreponse= "nbreponse".$j;
                    $name_type     = "typequestion".$j;

                    $question_test = new QuestionTest();
                    $codequestion = genCode("questtest", 100000000, 999999999);
                    $intitule = isCorrect($request->request->get($name_intitule));
                    $nbreponse = $request->request->get($name_nbreponse);
                    $type = $request->request->get($name_type);

                    $question_test->setCodeQuestion($codequestion)
                                ->setCodeQte($codeqte)
                                ->setIntitule($intitule)
                                ->setNbReponse($nbreponse)
                                ->setType($type);
                    $manager->persist($question_test);
                    $manager->flush();
                }

                return $this->redirectToRoute('test_qte', ['etape' => 3]);

            }
            if (!isset($questions)) {
                $questions = [];
            }

            return $this->render('creation_questionnaire/test.html.twig', [
                'etape' => $etape,
                'questions' => $questions
            ]);
        }elseif ($etape == 3) {
            $codeuser = $this->getUser()->getCodeUser();
            $repo1 = $manager->getRepository(Questionnaire::class);
            $qtes = $repo1->findByCodeClient($codeuser);

            $dernier_qte = end($qtes);
            $codeqte = $dernier_qte->getCodeQte();
            $nbquestion = $dernier_qte->getNbQuestion();

            $repo2 = $manager->getRepository(QuestionTest::class);
            $quests = $repo2->findByCodeQte($codeqte);
            $nbquests = count($quests);

            if ($nbquests == $nbquestion) {
                for ($i=0; $i < $nbquestion; $i++) {
                    $quest = $quests[$i];
                    $intitule_question = $quest->getIntitule();
                    $nbreponse = $quest->getNbReponse();
                    for ($j=0; $j < $nbreponse; $j++) {
                        $a = $i + 1;
                        $b = $j + 1;
                        $name_intitule = "intitule".$a.$b;
                        $noquestion = $a;
                        $noreponse  = $b;

                        //$rep["intitule_question"] = $intitule_question;
                        $rep["noquestion"] = $noquestion;
                        $rep["noreponse"] = $noreponse;
                        $rep["intitule_reponse"] = $name_intitule;

                        $repquest[$j] = $rep;

                        $inforep["noquestion"] = $noquestion;
                        $inforep["intitule_question"] = $intitule_question;
                        $inforep["reponse"] = $repquest;

                        //$repquest["intitule_question"] = $intitule_question;
                    }
                    $reponses[$i] = $inforep;
                }
            }else{
                $reponses = [];
            }

            if ($request->request->count() > 0) {
                for ($i=0; $i < $nbquestion; $i++) {
                    $quest = $quests[$i];
                    $codequestion = $quest->getCodeQuestion();
                    $nbreponse = $quest->getNbReponse();
                    for ($j=0; $j < $nbreponse; $j++) {
                        $a = $i + 1;
                        $b = $j + 1;
                        $name_intitule = "intitule".$a.$b;

                        $reponse = new ReponseTest();

                        $intitule = isCorrect($request->request->get($name_intitule));
                        $codereponse = genCode("reptest", 100000000000, 999999999999);
                        $default_score = 0;

                        $reponse->setCodeReponse($codereponse)
                                ->setCodeQuestion($codequestion)
                                ->setIntitule($intitule)
                                ->setScore($default_score);
                        $manager->persist($reponse);
                        $manager->flush();
                    }
                }
                return $this->redirectToRoute('test_qte', ['etape' => 4]);
            }

            return $this->render('creation_questionnaire/test.html.twig', [
                'etape' => $etape,
                'reponses' => $reponses
            ]);
        }elseif ($etape == 4) {

            $codeuser = $this->getUser()->getCodeUser();
            $repo = $manager->getRepository(Questionnaire::class);
            $qtes = $repo->findByCodeClient($codeuser);

            $dernier_qte = end($qtes);
            $codeqte = $dernier_qte->getCodeQte();

            $style = $dernier_qte->getStyleDeFond();

            if ($style == "geasscolor") {
                $typestyle["nb"] = 1;
                $color[0] = "red";
                $color[1] = "green";
                $color[2] = "blue-gradient";
                $color[3] = "orange";
                $color[4] = "yellow";
                $color[5] = "grey";
                $color[6] = "pink";
                $color[7] = "brown";
                $color[9] = "warm-flame-gradient";
                $color[10] = "night-fade-gradient";
                $color[11] = "spring-warmth-gradient";
                $color[12] = "juicy-peach-gradient";
                $color[13] = "young-passion-gradient";
                $color[14] = "rainy-ashville-gradient";
                $color[15] = "sunny-morning-gradient";
                $color[16] = "lady-lips-gradient";
                $color[17] = "winter-neva-gradient";
                $color[18] = "frozen-dreams-gradient";
                $color[19] = "dusty-grass-gradient";
                $color[20] = "tempting-azure-gradient";
                $color[21] = "heavy-rain-gradient";
                $color[22] = "amy-crisp-gradient";
                $color[23] = "mean-fruit-gradient";
                $color[24] = "deep-blue-gradient";
                $color[25] = "ripe-malinka-gradient";
                $color[26] = "cloudy-knoxville-gradient";
                $color[28] = "morpheus-den-gradient";
                $color[29] = "rare-wind-gradient";
                $color[30] = "near-moon-gradient";
                $color[31] = "peach-gradient";
                $color[32] = "aqua-gradient";
                $color[33] = "purple-gradient";

                $typestyle["color"] = $color;

            }elseif ($style == "geasspicture") {
                $typestyle["nb"] = 2;
            }else {
                return $this->redirectToRoute('logout');
            }

            return $this->render('creation_questionnaire/test.html.twig', [
                'etape' => $etape,
                'typestyle' => $typestyle
            ]);
        }else{
            return $this->redirectToRoute('compte');
        }

        //return $this->render('creation_questionnaire/test/etape1.html.twig');
    }

    /**
     * @Route("/compte/nouveau/form/{etape}", name="form_qte")
     */
    public function QteForm($etape = 1, Request $request, ObjectManager $manager)
    {
        function isCorrect($var){
            $var = htmlspecialchars($var);
            //$var = mysql_real_escape_string($var);

            return $var;
        }

        function genCode($mot, $nb_debut, $nb_fin){
            $nb = rand($nb_debut, $nb_fin);
            $code = $mot."-".$nb;

            return $code;
        }

        if ($etape == 1) {

            if ($request->request->count() > 0) {
                $qte = new Questionnaire();
                $qte_form = new QuestionnaireForm();

                $codeqte = genCode("qteform", 100000, 999999);
                $type    = "formulaire";
                $titre   = isCorrect($request->request->get('titre'));
                $nbquestion = intval($request->request->get('nbquestion'));
                $validite   = intval($request->request->get('validite'));
                $style      = $request->request->get('style');
                $description  = isCorrect($request->request->get('description'));
                $confirmation = isCorrect($request->request->get('confirmation'));
                $domaine      = isCorrect($request->request->get('domaine'));
                if ($confirmation == "") {
                    $confirmation = "Merci bien, votre/vos réponse(s) ont bien été enregistrée(s)";
                }
                $codeclient = $this->getUser()->getCodeUser();

                $qte->setCodeQte($codeqte)
                    ->setCodeClient($codeclient)
                    ->setTitre($titre)
                    ->setNbQuestion($nbquestion)
                    ->setType($type)
                    ->setConfirmation($confirmation);
                $qte_form->setCodeQte($codeqte)
                        ->setCodeClient($codeclient)
                        ->setDescription($description)
                        ->setDureeValidite($validite)
                        ->setDomaineActivite($domaine);
                $manager->persist($qte);
                $manager->persist($qte_form);
                $manager->flush();

                return $this->redirectToRoute('form_qte', ['etape' => 2]);
            }
            else{
                return $this->render('creation_questionnaire/form.html.twig', [
                    'etape' => $etape
                ]);
            }
        }elseif ($etape == 2) {

            $codeuser = $this->getUser()->getCodeUser();
            $repo = $manager->getRepository(Questionnaire::class);
            $qtes = $repo->findByCodeClient($codeuser);

            $dernier_qte = end($qtes);
            $codeqte = $dernier_qte->getCodeQte();
            $nbquestion = $dernier_qte->getNbQuestion();

            for ($i=0; $i < $nbquestion; $i++) {
                $j = $i + 1;
                $noquestion = $j;
                $name_intitule = "intitule".$j;
                $name_obligation= "obligation".$j;
                $name_type     = "typequestion".$j;

                $question["noquestion"]    = $noquestion;
                $question["intitule"]       = $name_intitule;
                $question["obligation"]      = $name_obligation;
                $question["typequestion"]   = $name_type;

                $questions[$i] = $question;

            }



            if ($request->request->count() > 0) {

                for ($i=0; $i < $nbquestion; $i++) {
                    $j = $i + 1;
                    $name_intitule = "intitule".$j;
                    $name_obligation= "obligation".$j;
                    $name_type     = "typequestion".$j;

                    $question_form = new QuestionForm();
                    $codequestion = genCode("questform", 100000000, 999999999);
                    $intitule = isCorrect($request->request->get($name_intitule));
                    $nbreponse = 0;
                    $obligation = $request->request->get($name_obligation);
                    $type = $request->request->get($name_type);

                    //tableau recueillant tous les types choisis
                    $tabtype[$i] = $type;

                    $question_form->setCodeQuestion($codequestion)
                                ->setCodeQte($codeqte)
                                ->setIntitule($intitule)
                                ->setNbReponse($nbreponse)
                                ->setObligation($obligation)
                                ->setType($type);
                    $manager->persist($question_form);
                    $manager->flush();
                }
                dump($request);

                function presence($tab){
                    if (in_array("qcu",$tab) || in_array("qcm",$tab) || in_array("liste",$tab)) {
                        return true;
                    }else {
                        return false;
                    }
                }

                $presence = presence($tabtype);


                if ($presence == true) {
                    return $this->redirectToRoute('form_qte', ['etape' => 3]);
                }else {
                    return $this->redirectToRoute('form_qte', ['etape' => 5]);
                }

            }
            if (!isset($questions)) {
                $questions = [];
            }

            return $this->render('creation_questionnaire/form.html.twig', [
                'etape' => $etape,
                'questions' => $questions
            ]);
        }elseif ($etape == 3) {

            $codeuser = $this->getUser()->getCodeUser();
            $repo1 = $manager->getRepository(Questionnaire::class);
            $qtes = $repo1->findByCodeClient($codeuser);

            $dernier_qte = end($qtes);
            $codeqte = $dernier_qte->getCodeQte();
            $nbquestion = $dernier_qte->getNbQuestion();

            $repo2 = $manager->getRepository(QuestionForm::class);
            $quests = $repo2->findByCodeQte($codeqte);

            function trierQuestion($tab_enter){

                $valtype = ["qcu", "qcm", "liste"];
                $a = 0;
                foreach ($tab_enter as $tab) {
                    $type = $tab->getType();
                    if (in_array($type, $valtype)) {
                        $tab_out[$a] = $tab;
                        $a++;
                    }
                }
               return $tab_out;
            }

            $quests   = trierQuestion($quests);
            $nbquests = count($quests);

            if ($request->request->count() > 0) {
                for ($i=0; $i < $nbquests; $i++) {
                    $j = $i + 1;
                    $quest = $quests[$i];
                    $codequestion = $quest->getCodeQuestion();
                    $name_nbreponse = "nbreponse".$j;

                    $question = $manager->getRepository(QuestionForm::class)
                                        ->findByCodeQuestion($codequestion);
                    $nbreponse = intval($request->request->get($name_nbreponse));


                    $question[0]->setNbReponse($nbreponse);
                    $manager->flush();
                    //die();
                }
                return $this->redirectToRoute('form_qte', ['etape' => 4]);
            }else{
                for ($i=0; $i < $nbquests; $i++) {
                    $quest = $quests[$i];
                    $intitule_question = $quest->getIntitule();
                    $type_question = $quest->getType();
                    $j = $i + 1;
                    $name_nbreponse = "nbreponse".$j;

                    $question["noquestion"] = $j;
                    $question["intitule_question"] = $intitule_question;
                    $question["type_question"]     = $type_question;
                    $question["name_nbreponse"]    = $name_nbreponse;

                    $questions[$i] = $question;
                }

                return $this->render('creation_questionnaire/form.html.twig', [
                    'etape' => $etape,
                    'questions' => $questions
                ]);
            }


        }elseif ($etape == 4) {
            $codeuser = $this->getUser()->getCodeUser();
            $repo1 = $manager->getRepository(Questionnaire::class);
            $qtes = $repo1->findByCodeClient($codeuser);

            $dernier_qte = end($qtes);
            $codeqte = $dernier_qte->getCodeQte();
            $nbquestion = $dernier_qte->getNbQuestion();

            $repo2 = $manager->getRepository(QuestionForm::class);
            $quests = $repo2->findByCodeQte($codeqte);

            function trierQuestion($tab_enter){

                $valtype = ["qcu", "qcm", "liste"];
                $a = 0;
                foreach ($tab_enter as $tab) {
                    $type = $tab->getType();
                    if (in_array($type, $valtype)) {
                        $tab_out[$a] = $tab;
                        $a++;
                    }
                }
               return $tab_out;
            }

            $quests   = trierQuestion($quests);
            $nbquests = count($quests);




            if ($request->request->count() > 0) {
                for ($i=0; $i < $nbquestion; $i++) {
                    $quest = $quests[$i];
                    $codequestion = $quest->getCodeQuestion();
                    $nbreponse = $quest->getNbReponse();
                    for ($j=0; $j < $nbreponse; $j++) {
                        $a = $i + 1;
                        $b = $j + 1;
                        $name_intitule = "intitule".$a.$b;

                        $reponse = new ReponseFormTypeA();

                        $intitule = isCorrect($request->request->get($name_intitule));
                        $codereponse = genCode("repform", 100000000000, 999999999999);

                        $reponse->setCodeReponse($codereponse)
                                ->setCodeQuestion($codequestion)
                                ->setIntitule($intitule);
                        $manager->persist($reponse);
                        $manager->flush();
                    }
                }
                return $this->redirectToRoute('form_qte', ['etape' => 5]);
            }else {

                for ($i=0; $i < $nbquests; $i++) {
                    $quest = $quests[$i];
                    $intitule_question = $quest->getIntitule();
                    $nbreponse = $quest->getNbReponse();
                    dump($quest);
                    dump($nbreponse);
                    for ($j=0; $j < $nbreponse; $j++) {
                        $a = $i + 1;
                        $b = $j + 1;
                        $name_intitule = "intitule".$a.$b;
                        $noquestion = $a;
                        $noreponse  = $b;

                        //$rep["intitule_question"] = $intitule_question;
                        $rep["noquestion"] = $noquestion;
                        $rep["noreponse"] = $noreponse;
                        $rep["intitule_reponse"] = $name_intitule;

                        $repquest[$j] = $rep;

                        $inforep["noquestion"] = $noquestion;
                        $inforep["intitule_question"] = $intitule_question;
                        $inforep["reponse"] = $repquest;

                        //$repquest["intitule_question"] = $intitule_question;
                    }
                        $reponses[$i] = $inforep;
                }

                return $this->render('creation_questionnaire/form.html.twig', [
                    'etape' => $etape,
                    'reponses' => $reponses
                ]);
            }


        }elseif ($etape == 5) {
            $codeuser = $this->getUser()->getCodeUser();
            $repo = $manager->getRepository(Questionnaire::class);
            $qtes = $repo->findByCodeClient($codeuser);

            $dernier_qte = end($qtes);
            $codeqte = $dernier_qte->getCodeQte();

            $style = $dernier_qte->getStyleDeFond();

            if ($style == "geasscolor") {
                $typestyle["nb"] = 1;
                $color[0] = "red";
                $color[1] = "green";
                $color[2] = "blue-gradient";
                $color[3] = "orange";
                $color[4] = "yellow";
                $color[5] = "grey";
                $color[6] = "pink";
                $color[7] = "brown";
                $color[9] = "warm-flame-gradient";
                $color[10] = "night-fade-gradient";
                $color[11] = "spring-warmth-gradient";
                $color[12] = "juicy-peach-gradient";
                $color[13] = "young-passion-gradient";
                $color[14] = "rainy-ashville-gradient";
                $color[15] = "sunny-morning-gradient";
                $color[16] = "lady-lips-gradient";
                $color[17] = "winter-neva-gradient";
                $color[18] = "frozen-dreams-gradient";
                $color[19] = "dusty-grass-gradient";
                $color[20] = "tempting-azure-gradient";
                $color[21] = "heavy-rain-gradient";
                $color[22] = "amy-crisp-gradient";
                $color[23] = "mean-fruit-gradient";
                $color[24] = "deep-blue-gradient";
                $color[25] = "ripe-malinka-gradient";
                $color[26] = "cloudy-knoxville-gradient";
                $color[28] = "morpheus-den-gradient";
                $color[29] = "rare-wind-gradient";
                $color[30] = "near-moon-gradient";
                $color[31] = "peach-gradient";
                $color[32] = "aqua-gradient";
                $color[33] = "purple-gradient";

                $typestyle["color"] = $color;

            }elseif ($style == "geasspicture") {
                $typestyle["nb"] = 2;
            }else {
                return $this->redirectToRoute('logout');
            }

            return $this->render('creation_questionnaire/form.html.twig', [
                'etape' => $etape,
                'typestyle' => $typestyle
            ]);
        }else{
            return $this->redirectToRoute('compte');
        }

        return $this->render('creation_questionnaire/form.html.twig', [
            'etape' => $etape,
        ]);
    }

    /**
     * @Route("/compte/nouveau/{typeqte}/4/{nomCouleur}", name="qte_couleur")
     */
    public function CouleurQte($typeqte, $nomCouleur, ObjectManager $manager)
    {
        if ($typeqte == "sond") {
            $codeuser = $this->getUser()->getCodeUser();
            $repo = $manager->getRepository(Questionnaire::class);
            $qtes = $repo->findByCodeClient($codeuser);

            $dernier_qte = end($qtes);
            $codeqte = $dernier_qte->getCodeQte();

            $dernier_qte->setCouleurDeFond($nomCouleur);
            $manager->flush();
            return $this->redirectToRoute('compte');
        }elseif ($typeqte == "test") {
            $codeuser = $this->getUser()->getCodeUser();
            $repo = $manager->getRepository(Questionnaire::class);
            $qtes = $repo->findByCodeClient($codeuser);

            $dernier_qte = end($qtes);
            $codeqte = $dernier_qte->getCodeQte();

            $dernier_qte->setCouleurDeFond($nomCouleur);
            $manager->flush();
            return $this->redirectToRoute('compte');
        }elseif ($typeqte == "form") {
            $codeuser = $this->getUser()->getCodeUser();
            $repo = $manager->getRepository(Questionnaire::class);
            $qtes = $repo->findByCodeClient($codeuser);

            $dernier_qte = end($qtes);
            $codeqte = $dernier_qte->getCodeQte();

            $dernier_qte->setCouleurDeFond($nomCouleur);
            $manager->flush();
            return $this->redirectToRoute('compte');
        }else{
            return $this->redirectToRoute('logout');
        }
    }

    /**
     * @Route("/compte/nouveau/{typeqte}/4/{nomImage}", name="qte_image")
     */
/*    public function ImageQte($typeqte, $nomImage)
    {

    }
*/
}
