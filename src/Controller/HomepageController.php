<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomepageController extends AbstractController
{
    /**
     * @Route("/", name="home1")
     */
    public function index()
    {
        return $this->render('homepage/index1.html.twig');
    }

    /**
     * @Route("/accueil", name="home2")
     */
    public function homepage()
    {
        return $this->render('homepage/index2.html.twig');
    }
}
