<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use Doctrine\Common\Persistence\ObjectManager;

use App\Entity\Questionnaire;
use App\Entity\QuestionnaireSondage;
use App\Entity\QuestionnaireTest;
use App\Entity\QuestionnaireForm;

use App\Entity\QuestionSondage;
use App\Entity\QuestionTest;
use App\Entity\QuestionForm;

use App\Entity\ReponseSondage;
use App\Entity\ReponseTest;
use App\Entity\ReponseFormTypeA;
use App\Entity\ReponseFormTypeB;

class CompteController extends AbstractController
{
    /**
     * @Route("/compte", name="compte")
     */
    public function index(ObjectManager $manager)
    {
        $quests = $manager->getRepository(Questionnaire::class)
                          ->findAll();

            $a = 0;
            $questions = [];
            foreach ($quests as $tab) {
                $couleur = $tab->getCouleurDeFond();
                if ($couleur != "") {
                    $questions[$a] = $tab;
                    $a++;
                }
            }

        $i = 0;
        //dump($questions);
        $questionnaires = [];
        foreach ($questions as $question) {
            $typeqte = $question->getType();

            if ($typeqte == "sondage") {
                $questionnaire["codeqte"]   = $question->getCodeQte();
                $repo =  $manager->getRepository(QuestionnaireSondage::class)
                                ->findByCodeQte($questionnaire["codeqte"]);
                $questionnaire["titre"]     = $question->getTitre();
                $questionnaire["icone"]     = "fa-chart-pie";
                $questionnaire['type']      = $question->getType();
                $questionnaire["color"]     = "blue-gradient";
                $questionnaire["validite"]  = $repo[0]->getDureeValidite();
                $questionnaire["description"] = $repo[0]->getDescription();

            }elseif ($typeqte == "test") {
                $questionnaire["codeqte"]   = $question->getCodeQte();
                $repo =  $manager->getRepository(QuestionnaireTest::class)
                                ->findByCodeQte($questionnaire["codeqte"]);
                $questionnaire["titre"]     = $question->getTitre();
                $questionnaire["icone"]     = "fa-graduation-cap";
                $questionnaire['type']      = $question->getType();
                $questionnaire["color"]     = "purple-gradient";
                $questionnaire["validite"]  = $repo[0]->getDureeValidite();
                $questionnaire["description"] = "Il s'agit d'un questionnaire d'évaluation de
                                                ".$repo[0]->getDureeTest()." minutes genéré pour ".$repo[0]->getNbParticipant()." participants";

            }elseif ($typeqte == "formulaire") {
                $questionnaire["codeqte"]   = $question->getCodeQte();
                $repo =  $manager->getRepository(QuestionnaireForm::class)
                                ->findByCodeQte($questionnaire["codeqte"]);
                $questionnaire["titre"]     = $question->getTitre();
                $questionnaire["icone"]     = "fa-newspaper";
                $questionnaire['type']      = $question->getType();
                $questionnaire["color"]     = "peach-gradient";
                $questionnaire["validite"]  = $repo[0]->getDureeValidite();
                $questionnaire["description"] = $repo[0]->getDescription();

            }else {
                return $this->redirectToRoute('logout');
            }

            $questionnaires[$i] = $questionnaire;
            $i++;


        }

        //dump($questionnaires);
        return $this->render('compte/index.html.twig', [
            'questionnaires' => $questionnaires
        ]);
    }

    /**
     * @Route("/compte/fiche/{codeqte}", name="fiche")
     */
    public function ficheQte($codeqte, ObjectManager $manager)
    {
        $qte = $manager->getRepository(Questionnaire::class)
                      ->findByCodeQte($codeqte);
        $typeqte = $qte[0]->getType();

        if ($typeqte == "sondage") {
            $qte_sond = $manager->getRepository(QuestionnaireSondage::class)
                                ->findByCodeQte($codeqte);

            //recup des infos                    
            $titre = $qte[0]->getTitre();
            $descript = $qte_sond[0]->getDescription();
            $validite = $qte_sond[0]->getDureeValidite();
            $couleurfd= $qte[0]->getCouleurDeFond();

            $quests = $manager->getRepository(QuestionSondage::class)
                              ->findByCodeQte($codeqte);
            $i = 0;
            foreach ($quests as $quest) {
                $codequest = $quest->getCodeQuestion();
                $intitule  = $quest->getIntitule();
                $typequest = $quest->getType();

                $reps = $manager->getRepository(ReponseSondage::class)
                                ->findByCodeQuestion($codequest);
                $j = 0;
                foreach ($reps as $rep) {
                    $coderep = $rep->getCodeReponse();
                    $intirep = $rep->getIntitule();

                    $reponse["codereponse"] = $coderep;
                    $reponse["intireponse"] = $intirep;

                    $reponses[$j] = $reponse;

                    $j++;
                }


                if ($typequest == "qcu") {
                    $typeform = "radio";
                }elseif ($typequest == "qcm") {
                    $typeform = "checkbox";
                }else{
                    return $this->redirectToRoute('logout');
                }

                $question["codequestion"] = $codequest;
                $question["intitule"]     = $intitule;
                $question["typeform"]     = $typeform;
                $question["reponses"]      = $reponses;

                $questions[$i] = $question;

                $i++;

            }

            $questionnaire["codeqte"] = $codeqte;
            $questionnaire["titre"] = $titre;
            $questionnaire["typeqte"] = $typeqte;
            $questionnaire["description"] = $descript;
            $questionnaire["validite"] = $validite;
            $questionnaire["couleurfd"]= $couleurfd;
            $questionnaire["icone"] = "fa-chart-pie";
            $questionnaire["colorform"] = "blue-gradient";
            $questionnaire["questions"] = $questions;


        }elseif ($typeqte == "test") {
            $qte_test = $manager->getRepository(QuestionnaireTest::class)
                                ->findByCodeQte($codeqte);
            $titre = $qte[0]->getTitre();
            $descript = "Il s'agit d'un questionnaire d'évaluation de
                                                ".$qte_test[0]->getDureeTest()." minutes genéré pour ".$qte_test[0]->getNbParticipant()." participant(s)";
            $validite = $qte_test[0]->getDureeValidite();
            $couleurfd= $qte[0]->getCouleurDeFond();
            $test = $qte_test[0]->getDureeTest();

            $quests = $manager->getRepository(QuestionTest::class)
                              ->findByCodeQte($codeqte);
            $i = 0;
            foreach ($quests as $quest) {
                $codequest = $quest->getCodeQuestion();
                $intitule  = $quest->getIntitule();
                $typequest = $quest->getType();

                $reps = $manager->getRepository(ReponseTest::class)
                                ->findByCodeQuestion($codequest);
                $j = 0;
                foreach ($reps as $rep) {
                    $coderep = $rep->getCodeReponse();
                    $intirep = $rep->getIntitule();

                    $reponse["codereponse"] = $coderep;
                    $reponse["intireponse"] = $intirep;

                    $reponses[$j] = $reponse;

                    $j++;
                }


                if ($typequest == "qcu") {
                    $typeform = "radio";
                }elseif ($typequest == "qcm") {
                    $typeform = "checkbox";
                }else{
                    return $this->redirectToRoute('logout');
                }

                $question["codequestion"] = $codequest;
                $question["intitule"]     = $intitule;
                $question["typeform"]     = $typeform;
                $question["reponses"]      = $reponses;

                $questions[$i] = $question;

                $i++;

            }

            $questionnaire["codeqte"] = $codeqte;
            $questionnaire["titre"]   = $titre;
            $questionnaire["typeqte"] = $typeqte;
            $questionnaire["description"] = $descript;
            $questionnaire["validite"] = $validite;
            $questionnaire["couleurfd"]= $couleurfd;
            $questionnaire["test"]  = $test;
            $questionnaire["icone"] = "fa-graduation-cap";
            $questionnaire["colorform"] = "peach-gradient";
            $questionnaire["questions"]  = $questions;


        }elseif ($typeqte == "formulaire") {
            $qte_form = $manager->getRepository(QuestionnaireForm::class)
                                ->findByCodeQte($codeqte);
            //                  
            $titre = $qte[0]->getTitre();
            $descript = $qte_form[0]->getDescription();
            $validite = $qte_form[0]->getDureeValidite();
            $couleurfd= $qte[0]->getCouleurDeFond();
            $activite = $qte_form[0]->getDomaineActivite();

            $quests = $manager->getRepository(QuestionForm::class)
                              ->findByCodeQte($codeqte);
            $i = 0;
            foreach ($quests as $quest) {
                $codequest = $quest->getCodeQuestion();
                $intitule  = $quest->getIntitule();
                $typequest = $quest->getType();
                $obligation= $quest->getObligation();

                $valeurTypeAutorise = ["qcu", "qcm", "liste"];

                $reponses = [];

                if (in_array($typequest, $valeurTypeAutorise)) {

                    $reps = $manager->getRepository(ReponseFormTypeA::class)
                                ->findByCodeQuestion($codequest);
                    $j = 0;
                    foreach ($reps as $rep) {
                        $coderep = $rep->getCodeReponse();
                        $intirep = $rep->getIntitule();

                        $reponse["codereponse"] = $coderep;
                        $reponse["intireponse"] = $intirep;

                        $reponses[$j] = $reponse;

                        $j++;
                    }
                }

                if ($obligation == "oui") {
                    $obform = "required";
                }elseif ($obligation == "non") {
                    $obform = "";
                }else {
                    return $this->redirectToRoute('logout');
                }



                $question["codequestion"] = $codequest;
                $question["intitule"]     = $intitule;
                $question["obform"]   = $obform;
                $question["typequestion"] = $typequest;
                $question["reponses"]     = $reponses;

                $questions[$i] = $question;

                $i++;

            }

            $questionnaire["codeqte"] = $codeqte;
            $questionnaire["titre"]   = $titre;
            $questionnaire["typeqte"] = $typeqte;
            $questionnaire["activite"]= $activite; 
            $questionnaire["description"] = $descript;
            $questionnaire["validite"] = $validite;
            $questionnaire["couleurfd"]= $couleurfd;
            //$questionnaire["test"]  = $test;
            $questionnaire["icone"] = "fa-graduation-cap";
            $questionnaire["colorform"] = "purple-gradient";
            $questionnaire["questions"]  = $questions;
        }

        dump($questionnaire);
        return $this->render('compte/fiche.html.twig', [
            'questionnaire' => $questionnaire
        ]);
    }

}
