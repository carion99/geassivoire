<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class GestionQuestionnaireController extends AbstractController
{
    /**
     * @Route("/compte/gestionqte", name="gestion_qte")
     */
    public function index()
    {
        return $this->render('gestion_questionnaire/index.html.twig', [
            'controller_name' => 'GestionQuestionnaireController',
        ]);
    }
}
