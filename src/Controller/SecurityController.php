<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;


use App\Entity\CompteClient;
use App\Entity\User;

use App\Repository\UserRepository;


class SecurityController extends AbstractController
{
    /**
     * @Route("/security", name="security")
     */
    public function index()
    {
        return $this->render('security/index.html.twig', [
            'controller_name' => 'SecurityController',
        ]);
    }


    /**
     * @Route("/logout", name="logout")
     */
    public function logout()
    {

    }

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
      $this->encoder = $encoder;
    }

    /**
     * @Route("/register", name="register")
     */
    public function register(Request $request, ObjectManager $manager)
    {

        if ($request->request->count() > 0) {
             $compteclient = new CompteClient();


             function genCode($motcle)
             {
                 $nb  = rand(1000000, 9999999);
                 $code = $motcle.'-'.$nb;

                 return $code;
             }

             $user = new User();

             $password = $this->encoder->encodePassword($user, $request->request->get('password'));
             $code = genCode("client");
             $nom = $request->request->get('nom');
             $prenom = $request->request->get('prenom');
             $email = $request->request->get('email');
             $motdepasse = $request->request->get('motdepasse');
             $profession = $request->request->get('profession');
             $date = new \Datetime();
             $permission = "no";

             dump($request);


              $user->setUsername($email);
              $user->setRoles(['ROLE_CLIENT']);
              $user->setPassword($password);
              $user->setLastname(strtoupper($nom));
              $user->setFirstname(strtoupper($prenom));
              $user->setCodeUser($code);
              $user->setStatus('client');

              $manager->persist($user);

             $compteclient->setCodeCptClient($code)
                          ->setNom($nom)
                          ->setPrenom($prenom)
                          ->setEmail($email)
                          ->setMotdepasse($password)
                          ->setTravail($profession)
                          ->setDateregister($date)
                          ->setPermission($permission);

              $manager->persist($compteclient);

              $manager->flush();

              return $this->redirectToRoute('compte');

         }

      return $this->render('security/register.html.twig');
    }


    /**
     * @Route("/login", name="login")
     */
    public function login(Request $request, AuthenticationUtils $utils, ObjectManager $manager)
    {
      $error  = $utils->getLastAuthenticationError();
      $lastUsername = $utils->getLastUsername();

      $user = $this->getUser();
      if (isset($user)) {
          return $this->redirectToRoute('compte');
      }


      return $this->render('security/login.html.twig', [
          'error' => $error,
          'last_username' => $lastUsername
      ]);
    }
}
